package com.example.themoviedb.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.themoviedb.model.Result;

@Database(entities = {Result.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ResultDao resultDao();
}
