package com.example.themoviedb.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.themoviedb.model.Result;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;

@Dao
public interface ResultDao {
    @Query("SELECT * FROM result")
    Maybe<List<Result>> getAll();
    //
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable setAllResults(List<Result> results);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    Completable updateAllResults(List<Result> results);

    @Query("DELETE FROM result")
    Completable deleteAll();
}
