package com.example.themoviedb.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.example.themoviedb.R;
import com.example.themoviedb.adapter.MovieAdapter;
import com.example.themoviedb.database.DatabaseClient;
import com.example.themoviedb.databinding.MoviesBinding;
import com.example.themoviedb.model.Movies;
import com.example.themoviedb.model.Result;
import com.example.themoviedb.other.SharedPrefsManager;
import com.example.themoviedb.repository.UserRepository;
import com.example.themoviedb.viewmodel.MoviesViewModel;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableMaybeObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/*
API Key (v3 auth)
704a06995b5403e6f22f2d505ec5850e
https://api.themoviedb.org/3/movie/550?api_key=704a06995b5403e6f22f2d505ec5850e
https://api.themoviedb.org/3/movie/now_playing/?api_key=704a06995b5403e6f22f2d505ec5850e
https://api.themoviedb.org/3/search/movie/?api_key=704a06995b5403e6f22f2d505ec5850e
https://image.tmdb.org/t/p/w500/iZf0KyrE25z1sage4SYFLCCrMi9.jpg
 */
public class MoviesActivity extends AppCompatActivity {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private List<Result> resultsWithDatabase;
    private CompositeDisposable disposable;
    private Disposable disposableDatabase;
    private MoviesBinding binding;
    private MoviesViewModel viewModel;
    private List<MoviesViewModel> results;
    private MovieAdapter adapter;
    private List<String> searchMoviesList;
    private final String TAG = "MovieActivity";
    private boolean isBack = false;
    private UserRepository userRepository;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        getSupportActionBar().setTitle("Movie!");
    }

    private void init(){
        SharedPrefsManager.initialize(this);
        uiThread = new Handler();
        results = new LinkedList<>();
        resultsWithDatabase = new LinkedList<>();
        disposable = new CompositeDisposable();
        initViewModel();
        initBinding();
//        if(results.isEmpty()) getDataWithDatabase();
//        loadData();
        initSearchView();
        viewModel.getProgressVisible().set(results.isEmpty());
    }
    private void initBinding(){
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setLifecycleOwner(this);
        binding.setViewmodel(viewModel);
    }

    private void initViewModel()
    {
        viewModel = new MoviesViewModel();
        ((BaseApplication) getApplication()).getAppComponent().inject(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MoviesViewModel.class);
        viewModel.getSearchMoviesListMutable().observe(this, searchList->{

        });
        viewModel.getModelMutableLiveData().observe(this, this::setBinding);
        viewModel.getClickMovie().observe(this, this::goToMovieDetails);
        userRepository = viewModel.getUserRepository();

    }

    private Handler uiThread;
    public void showToast(final String text) {
        uiThread.post(() -> Toast.makeText(this, text, Toast.LENGTH_LONG).show());
    }

    private void setDataToDatabase(List<Result> mResults){
        disposableDatabase = DatabaseClient
                .getInstance(this)
                .getAppDatabase()
                .resultDao()
                .setAllResults(mResults)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        Log.d(TAG,"complete");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, e.getMessage());
                    }
                });
    }
    private void updateDataToDatabase(List<Result> mResults){
        disposableDatabase = DatabaseClient
                .getInstance(this)
                .getAppDatabase()
                .resultDao()
                .updateAllResults(mResults)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        Log.d(TAG,"complete");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, e.getMessage());
                    }
                });
    }
    private void getDataWithDatabase(){
        disposableDatabase = DatabaseClient
                .getInstance(this)
                .getAppDatabase()
                .resultDao()
                .getAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableMaybeObserver<List<Result>>() {

                    @Override
                    public void onSuccess(List<Result> mResults) {
                        Log.d(TAG,"complete");
                        if(!mResults.isEmpty()){
                            resultsWithDatabase.clear();
                            resultsWithDatabase.addAll(mResults);
                            results.clear();
                            for (Result result: mResults) {
                                results.add(new MoviesViewModel(result));
                            }
                            viewModel.setResults(results);
                            viewModel.setFirst(true);
                        }
                        loadData();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG,"complete");
                        if(!viewModel.isFirst()) loadData();

                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG,"complete");

                    }

                });
    }

    private void loadData() {
        disposable.add(userRepository
                        .modelGetMovies()
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<Movies>() {
            @Override
            public void onSuccess(Movies movies) {
                if(!resultsWithDatabase.isEmpty()) updateDataToDatabase(movies.getResults());
                else setDataToDatabase(movies.getResults());
                results.clear();
                for (Result result: movies.getResults()) {
                    results.add(new MoviesViewModel(result));
                }
                viewModel.setResults(results);
                viewModel.setFirst(true);
            }

            @Override
            public void onError(Throwable e) {
//                getDataWithDatabase();
                if(resultsWithDatabase.isEmpty()) getDataWithDatabase();
                else loadData();
            }
        }));
    }

    private void searchMovie(String name) {
        disposable.add(userRepository
                        .modelSearchMovies(name)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<Movies>() {
                            @Override
                            public void onSuccess(Movies movies) {
                                results.clear();
                                for (Result result: movies.getResults()) {
                                    results.add(new MoviesViewModel(result));
                                }
                                viewModel.createStringMovieList(results);
                                viewModel.setResults(results);
                                setBinding(results);
                            }

                            @Override
                            public void onError(Throwable e) {
                                if(resultsWithDatabase.isEmpty()) loadData();
                            }
                        }));
    }
    private void initSearchView(){

        binding.search.setActivated(true);
        binding.search.setQueryHint("Type your keyword here");
        binding.search.onActionViewExpanded();
        binding.search.setIconified(false);
        binding.search.clearFocus();
        binding.search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(viewModel.oldText.getValue()!=null && viewModel.oldText.getValue().equals(newText)) return false;
                if(!newText.isEmpty()) searchMovie(newText);
                else loadData();
                adapter.getFilter().filter(newText);
                viewModel.oldText.setValue(newText);
                return false;
            }
        });
    }

    private void setBinding(List<MoviesViewModel> results){
        viewModel.getProgressVisible().set(results.isEmpty());
        if(results.isEmpty()){

            loadData();
        }else{
            adapter = new MovieAdapter(results, viewModel,viewModel.searchMoviesListMutable.getValue());
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(MoviesActivity.this));
            binding.recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }
    private void setBinding(){
        viewModel.getProgressVisible().set(results.isEmpty());
        adapter = new MovieAdapter(results, viewModel,viewModel.searchMoviesListMutable.getValue());
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(MoviesActivity.this));
        binding.recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    private void goToMovieDetails(MoviesViewModel moviesViewModel){
        if(moviesViewModel == null) return;
        if(moviesViewModel.isClick()){
            moviesViewModel.setClick(false);
            Intent intent = new Intent(MoviesActivity.this, DatailsMovieActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("viewModel", viewModel.getResult(viewModel.getClickMovie().getValue()));
            startActivity(intent);
            isBack = true;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        if(isBack){
//            isBack = false;
//            setBinding();
//        }
//        setBinding();
    }
}
