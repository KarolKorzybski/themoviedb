package com.example.themoviedb.view;

import android.app.Application;
import com.example.themoviedb.di.components.AppComponentMovies;
import com.example.themoviedb.di.components.DaggerAppComponentMovies;

public class BaseApplication extends Application {

    private AppComponentMovies appComponentMovies;


    @Override
    public void onCreate() {
        super.onCreate();
        appComponentMovies= DaggerAppComponentMovies.create();
    }

    public AppComponentMovies getAppComponent() {
        return appComponentMovies;
    }
}