package com.example.themoviedb.view;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import com.example.themoviedb.R;
import com.example.themoviedb.callback.DetailsMovieCallback;
import com.example.themoviedb.databinding.DetailsMovieBinding;
import com.example.themoviedb.model.Result;
import com.example.themoviedb.other.SharedPrefsManager;
import com.example.themoviedb.viewmodel.DetailsMovieViewModel;

import javax.inject.Inject;

public class DatailsMovieActivity extends AppCompatActivity implements DetailsMovieCallback {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private DetailsMovieBinding binding;
    private Result movie;
    private DetailsMovieViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }
    private Result getMovieAndPath(){

        Bundle extras = getIntent().getExtras();
        if(extras == null) return  null;
        else {
            Object object = extras.getSerializable("viewModel");
            if(object instanceof Result){
                return (Result) object;
            }else{
                return null;
            }
        }
    }

    private void saveStateStar(DetailsMovieViewModel viewModel){
        SharedPrefsManager
                .getInstance()
                .setBoolean(viewModel.getId() + "",viewModel.isStar());
    }
    private Boolean getStateStar(DetailsMovieViewModel viewModel){
        return SharedPrefsManager
                .getInstance()
                .getBoolean(viewModel.getId() + "");
    }
    private void init(){
        if(getMovieAndPath() !=null) movie = getMovieAndPath();
        ((BaseApplication) getApplication()).getAppComponent().inject(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailsMovieViewModel.class);
//        viewModel = new DetailsMovieViewModel(movie);
        viewModel.setResult(movie);
        viewModel.setStar(getStateStar(viewModel));
        binding = DataBindingUtil.setContentView(this, R.layout.activity_datails_movie);
        binding.setLifecycleOwner(this);
        binding.setCallback(this);
        binding.setViewmodel(viewModel);
        getSupportActionBar().setTitle(viewModel.getTitle());

    }

    @Override
    public void clickStar(DetailsMovieViewModel viewModel) {
        viewModel.setStar(!viewModel.isStar());
        saveStateStar(viewModel);
        binding.setViewmodel(viewModel);
    }

}
