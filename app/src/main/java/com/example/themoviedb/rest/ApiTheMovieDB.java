package com.example.themoviedb.rest;

import com.example.themoviedb.model.Movies;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ApiTheMovieDB {
    @Headers("Content-Type: application/json;charset=utf-8")
    @GET("/3/movie/now_playing/?api_key=704a06995b5403e6f22f2d505ec5850e")
    Single<Movies> getMovies();

    @GET("/3/search/movie?api_key=704a06995b5403e6f22f2d505ec5850e")
    Single<Movies> getSearchMovies(
            @Query("query") String query
    );
}
