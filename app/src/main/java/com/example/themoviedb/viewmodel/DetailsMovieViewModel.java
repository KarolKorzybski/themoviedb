package com.example.themoviedb.viewmodel;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.lifecycle.ViewModel;

import com.bumptech.glide.Glide;
import com.example.themoviedb.model.Result;
import com.example.themoviedb.repository.UserRepository;

import javax.inject.Inject;

public class DetailsMovieViewModel extends ViewModel {
    private String title;
    private String overview;
    private String posterPath;
    private Double voteAverage;
    private String releaseDate;
    private boolean isStar = false;
    private int id;
    private UserRepository userRepository;
    private final static String URL_ORIGINAL = "https://image.tmdb.org/t/p/original/";


    public DetailsMovieViewModel(Result result) {
        this.title = result.getTitle();
        this.overview = result.getOverview();
        this.posterPath = result.getPosterPath();
        this.voteAverage = result.getVoteAverage();
        this.releaseDate = result.getReleaseDate();
        this.isStar = result.isStar();
        this.id = result.getId();
    }
    public void setResult(Result result){
        this.title = result.getTitle();
        this.overview = result.getOverview();
        this.posterPath = result.getPosterPath();
        this.voteAverage = result.getVoteAverage();
        this.releaseDate = result.getReleaseDate();
        this.isStar = result.isStar();
        this.id = result.getId();
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Inject
    public DetailsMovieViewModel(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    public DetailsMovieViewModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @BindingAdapter("android:src")
    public static void loadImage(ImageView view, String imageUrl){
        Glide.with(view.getContext())
                .load(URL_ORIGINAL + imageUrl)
                .into(view);
    }


    public boolean isStar() {
        return isStar;
    }

    public void setStar(boolean star) {
        isStar = star;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }
}
