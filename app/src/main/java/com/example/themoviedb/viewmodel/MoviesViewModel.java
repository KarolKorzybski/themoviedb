package com.example.themoviedb.viewmodel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.themoviedb.model.Result;
import com.example.themoviedb.repository.UserRepository;
import com.mikhaellopez.circularimageview.CircularImageView;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

public class MoviesViewModel extends ViewModel {
    private final static String URL_ORIGINAL = "https://image.tmdb.org/t/p/original/";
    private final static String URL = "https://image.tmdb.org/t/p/w500/";
    private boolean isFirst = false;
    private boolean isClick = false;
    private boolean isStar = false;
    private ObservableBoolean progressVisible = new ObservableBoolean(true);
    private String posterPath;
    private String backdropPath;
    private String title;
    private String overview;
    private String releaseDate;
    private String image;
    private Double voteAverage;
    private List<MoviesViewModel> viewModels = new LinkedList<>();
    private List<String> searchMoviesList = new LinkedList<>();
    private int id;
    public MutableLiveData<List<MoviesViewModel>> liveData = new MutableLiveData<>();
    public MutableLiveData<List<String>> searchMoviesListMutable = new MutableLiveData<>();
    public MutableLiveData<String> oldText = new MutableLiveData<>();
    public MutableLiveData<MoviesViewModel> clickMovie = new MutableLiveData<>();
    private UserRepository userRepository;

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public MutableLiveData<String> getOldText() {
        return oldText;
    }

    public void setOldText(MutableLiveData<String> oldText) {
        this.oldText = oldText;
    }

    public ObservableBoolean getProgressVisible() {
        return progressVisible;
    }

    public void setProgressVisible(ObservableBoolean progressVisible) {
        this.progressVisible = progressVisible;
    }

    public MutableLiveData<List<String>> getSearchMoviesListMutable() {
        searchMoviesListMutable.setValue(searchMoviesList);
        return searchMoviesListMutable;
    }

    public void setSearchMoviesListMutable(MutableLiveData<List<String>> searchMoviesListMutable) {
        this.searchMoviesListMutable = searchMoviesListMutable;
    }

    @Inject
    public MoviesViewModel(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    public MoviesViewModel(Result result) {
        this.posterPath = result.getPosterPath();
        this.backdropPath = result.getBackdropPath();
        this.title = result.getTitle();
        this.overview = result.getOverview();
        this.releaseDate = result.getReleaseDate();
        this.image = result.getPosterPath();
        this.voteAverage = result.getVoteAverage();
        this.isStar = result.isStar();
        this.id = result.getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isStar() {
        return isStar;
    }

    public void setStar(boolean star) {
        isStar = star;
    }

    public MoviesViewModel() {
        viewModels = new LinkedList<>();
        searchMoviesList = new LinkedList<>();
    }

    public List<String> getSearchMoviesList() {
        return searchMoviesList;
    }
    public void createStringMovieList(List<MoviesViewModel> viewModels){
        searchMoviesList.clear();
        for (MoviesViewModel viewModel: viewModels){
            searchMoviesList.add(viewModel.title);
        }
        searchMoviesListMutable.setValue(searchMoviesList);
    }

    public void setSearchMoviesList(List<String> searchMoviesList) {
        this.searchMoviesList = searchMoviesList;
    }

    @BindingAdapter("android:src")
    public static void loadImage(CircularImageView view, String imageUrl){
        Glide.with(view.getContext())
                .load(URL + imageUrl)
                .apply(RequestOptions.centerCropTransform())
                .into(view);
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public MutableLiveData<List<MoviesViewModel>> getModelMutableLiveData() {
        liveData.setValue(viewModels);
        return liveData;
    }
    public void setResults(List<MoviesViewModel> results){
        viewModels.clear();
        viewModels.addAll(results);
        getModelMutableLiveData().setValue(viewModels);
    }

    public MutableLiveData<MoviesViewModel> getClickMovie() {
        return clickMovie;
    }

    public void setClickMovie(MutableLiveData<MoviesViewModel> clickMovie) {
        this.clickMovie = clickMovie;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Result getResult(MoviesViewModel viewModel){
        Result result = new Result();
        result.setTitle(viewModel.title);
        result.setOverview(viewModel.overview);
        result.setPosterPath(viewModel.getPosterPath());
        result.setVoteAverage(viewModel.getVoteAverage());
        result.setReleaseDate(viewModel.releaseDate);
        result.setId(viewModel.getId());
        return result;
    }
}
