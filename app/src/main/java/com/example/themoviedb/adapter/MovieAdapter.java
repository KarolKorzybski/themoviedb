package com.example.themoviedb.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.themoviedb.R;
import com.example.themoviedb.callback.MoviesCallback;
import com.example.themoviedb.databinding.AdapterRow;
import com.example.themoviedb.other.SharedPrefsManager;
import com.example.themoviedb.viewmodel.MoviesViewModel;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieView> implements MoviesCallback, Filterable {
    private List<MoviesViewModel> arrayList;
    private LayoutInflater layoutInflater;
    private MoviesViewModel viewModel;
    private ValueFilter valueFilter;
    List<String> mData;
    List<String> mStringFilterList;

    public MovieAdapter(List<MoviesViewModel> results, MoviesViewModel viewModel, List<String> cancel_type) {
        this.arrayList = results;
        this.viewModel = viewModel;
        mData=cancel_type;
        mStringFilterList = cancel_type;
    }

    @Override
    public MovieView onCreateViewHolder(final ViewGroup viewGroup, int i) {


        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(viewGroup.getContext());
        }
        AdapterRow adapterRow = DataBindingUtil.inflate(layoutInflater, R.layout.row_movie, viewGroup, false);
        adapterRow.setCallback(this);
        return new MovieView(adapterRow);
    }

    @Override
    public void onBindViewHolder(MovieView viewHolder, int position) {

        MoviesViewModel result = arrayList.get(position);
        result.setStar(getStateStar(result));
        viewHolder.bind(result);
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    @Override
    public void clickMovie(MoviesViewModel clickViewModel) {
        clickViewModel.setClick(true);
        viewModel.clickMovie.setValue(clickViewModel);
    }

    @Override
    public void clickStar(MoviesViewModel viewModel) {
        viewModel.setStar(!viewModel.isStar());
        saveStateStar(viewModel);
        notifyDataSetChanged();
    }
    private void saveStateStar(MoviesViewModel viewModel){
        SharedPrefsManager
                .getInstance()
                .setBoolean(viewModel.getId() + "",viewModel.isStar());
    }
    private Boolean getStateStar(MoviesViewModel viewModel){
        return SharedPrefsManager
                .getInstance()
                .getBoolean(viewModel.getId() + "");
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                List<String> filterList = new ArrayList<>();
                for (int i = 0; i < mStringFilterList.size(); i++) {
                    if ((mStringFilterList.get(i).toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(mStringFilterList.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            mData = (List<String>) results.values;
            notifyDataSetChanged();
        }
    }

    public class MovieView extends RecyclerView.ViewHolder {

        private AdapterRow adapterRow;

        public MovieView(AdapterRow adapterRow) {
            super(adapterRow.getRoot());
            this.adapterRow = adapterRow;
        }

        public void bind(MoviesViewModel result) {
            this.adapterRow.setViewmodel(result);
            adapterRow.executePendingBindings();
        }

        public AdapterRow getAdapterRow() {
            return adapterRow;
        }

    }
}