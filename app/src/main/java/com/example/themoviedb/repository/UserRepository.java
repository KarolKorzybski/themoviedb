package com.example.themoviedb.repository;
import com.example.themoviedb.model.Movies;
import com.example.themoviedb.rest.ApiTheMovieDB;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Single;

public class UserRepository {

    private ApiTheMovieDB api;

    @Inject
    public UserRepository(ApiTheMovieDB api) {
        this.api = api;
    }

    public Single<Movies> modelGetMovies() {
        return api.getMovies();
    }
    public Single<Movies> modelSearchMovies(String query) {
        return api.getSearchMovies(query);
    }
}
