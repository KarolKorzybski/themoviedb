package com.example.themoviedb.di.components;

import com.example.themoviedb.di.modules.ContextModule;
import com.example.themoviedb.di.modules.NetworkModule;
import com.example.themoviedb.view.DatailsMovieActivity;
import com.example.themoviedb.view.MoviesActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class, ContextModule.class})
public interface AppComponentMovies {
    void inject(DatailsMovieActivity activity);
    void inject(MoviesActivity activity);
}