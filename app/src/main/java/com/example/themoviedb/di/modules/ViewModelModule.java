package com.example.themoviedb.di.modules;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.example.themoviedb.di.ViewModelKey;
import com.example.themoviedb.viewmodel.DetailsMovieViewModel;
import com.example.themoviedb.viewmodel.MoviesViewModel;
import com.example.themoviedb.viewmodel.ViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MoviesViewModel.class)
    abstract ViewModel bindViewModel(MoviesViewModel viewModel);
    //
    @Binds
    @IntoMap
    @ViewModelKey(DetailsMovieViewModel.class)
    abstract ViewModel bindViewModelDetails(DetailsMovieViewModel viewModel);

    @Binds
    abstract ViewModelProvider.Factory bindFactory(ViewModelFactory factory);

}
