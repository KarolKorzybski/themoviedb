package com.example.themoviedb.callback;

import com.example.themoviedb.viewmodel.DetailsMovieViewModel;

public interface DetailsMovieCallback {
    void clickStar(DetailsMovieViewModel viewModel);
}
