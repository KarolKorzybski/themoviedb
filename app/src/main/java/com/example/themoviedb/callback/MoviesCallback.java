package com.example.themoviedb.callback;


import com.example.themoviedb.viewmodel.MoviesViewModel;

public interface MoviesCallback {
    void clickMovie(MoviesViewModel viewModel);
    void clickStar(MoviesViewModel viewModel);
}
